
# Cloudalbania DEB repository

## Use the repo

To install deb packages, add new repository to `/etc/apt/sources.list.d/cloudalbania.list`:

```text
deb [trusted=yes arch=amd64] https://cloudalbania.fury.site/apt/ /
```

After that you can install packages as any other software package. Example for zoom:

```text
sudo apt-get update
sudo apt-get install zoom
```

## List of current packages

### Zoom

#### Zoom Release Notes for Linux URL

https://support.zoom.us/hc/en-us/articles/205759689

Zoom's GPG key retrieved from here:

https://zoom.us/download

uploaded in http://keyserver.ubuntu.com/

#### GPG Key

```text
rsa4096/59c86188e22abb19bd5540477b04a1b8dd79b481
```

#### GPG Key location

```text
http://keyserver.ubuntu.com/pks/lookup?search=59c86188e22abb19bd5540477b04a1b8dd79b481&fingerprint=on&op=index
```
