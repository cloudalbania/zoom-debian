#!/bin/bash

# Adding some bash best practices
set -euox pipefail

# Prepare OS
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get install -y wget curl
# gpg gnupg xz-utils gettext

# Release info for Linux - get latest version
# https://support.zoom.us/hc/en-us/articles/205759689

# Get latest zoom client version from zoom CDN
wget https://zoom.us/client/latest/zoom_amd64.deb
zoom_latest_version=$(dpkg -I zoom_amd64.deb | grep Version | awk '{print $2}')


# Check existing version in repo, if present by making the repo availabe locally
echo "deb [trusted=yes] https://apt.fury.io/cloudalbania/ /" > /etc/apt/sources.list.d/fury.list
apt-get update
zoom_version_in_repo=$(apt-cache show zoom | grep Version | awk '{print $2}' | head -1)

# Define Gemfury Push URL
# Variable GEMFURY_PUSH_TOKEN is defined in CI/CD variables
PUSH_URL=https://$GEMFURY_PUSH_TOKEN@push.fury.io/cloudalbania/

# Main script logic
if [[ "$zoom_latest_version" != "$zoom_version_in_repo" ]] ; then
  echo "New available Zoom client version: $zoom_latest_version. Updating the current available version in the repo: $zoom_version_in_repo"
  echo "Updating the repo with the new version"
  curl -F package=@zoom_amd64.deb $PUSH_URL
else
  echo "The current version in repo: $zoom_version_in_repo is the same as the one available upstream: $zoom_latest_version"
  echo "Not pushing anything in the repo"
fi

# Cleanup
echo "Cleaning up"
rm -f zoom_amd64.deb
rm -rf /etc/apt/sources.list.d/fury.list