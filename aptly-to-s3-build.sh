#!/bin/bash

set -x

# Release info for Linux - get latest version
# https://support.zoom.us/hc/en-us/articles/205759689

# get latest zoom client version
# wget https://zoom.us/client/latest/zoom_amd64.deb
# zoom_latest_version=$(dpkg -I zoom_amd64.deb | grep Version | awk '{print $2}')
# rm -f zoom_amd64.deb

# check existing versions, if present
# zoom_exisiting version=curl

# zoom_versions=$(cat zoom_client_versions.ini)



#zoom_client_version="5.14.0.1720"
zoom_client_versions=("5.15.10.6882" "5.15.11.7239" "5.15.12.7665" "5.16.2.8828")
zoom_client_file="zoom_amd64.deb"
zoom_client_server_url="https://zoom.us/client/"
zoom_client_download_dir="downloaded_package_files"

aptly_snapshot_version=$(for zoom_client_version in "${zoom_client_versions[@]}" ; do echo $zoom_client_version; done | sort -Vr | head -n1)

mkdir -p $zoom_client_download_dir

for zoom_client_version in "${zoom_client_versions[@]}" 
do
  zoom_client_download_url=$zoom_client_server_url/$zoom_client_version/$zoom_client_file
  # use a different file name locally
  zoom_client_local_file="zoom_"$zoom_client_version"_amd64.deb"
  wget $zoom_client_download_url -O $zoom_client_download_dir/$zoom_client_local_file
done

# build release info

envsubst < ./aptly/aptly.conf > ~/.aptly.conf

grep S3PublishEndpoints ~/.aptly.conf -A 3

aptly repo create -component="main" -distribution="focal" cloudalbania-release
aptly repo add cloudalbania-release downloaded_package_files/
aptly snapshot create cloudalbania-$aptly_snapshot_version from repo cloudalbania-release
aptly repo show -with-packages cloudalbania-release

aptly snapshot list

aptly snapshot show cloudalbania-$aptly_snapshot_version

aptly publish -architectures="amd64" -component="main" -distribution="focal" -skip-signing snapshot cloudalbania-$zoom_client_version s3:deb.cloudalbania.com:

